#define RX 11
#define TX 10

#include <Adafruit_MotorShield.h>
#include <SoftwareSerial.h>

SoftwareSerial bt(TX, RX);

Adafruit_MotorShield motorShield = Adafruit_MotorShield();
Adafruit_DCMotor *l = motorShield.getMotor(1);
Adafruit_DCMotor *r = motorShield.getMotor(2);

void setup() {
  bt.begin(9600);
  bt.println("AT+NAMESMARS");

  motorShield.begin();
  l->setSpeed(127);
  l->run(FORWARD);
}

void loop() {
  if (bt.available()) {
    char c = bt.read();
  }
}
