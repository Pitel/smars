package org.kalabovi.smars

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter

class PagerAdapter(activity: FragmentActivity): FragmentStateAdapter(activity) {
    override fun getItemCount() = 2

    override fun createFragment(position: Int) = when(position) {
        0 -> JoystickFragment()
        1 -> TreadsFragment()
        else -> Fragment()
    }
}