package org.kalabovi.smars

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothSocket
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.*
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.actor
import kotlinx.coroutines.channels.consumeEach
import timber.log.Timber
import java.util.*

@OptIn(ObsoleteCoroutinesApi::class, ExperimentalCoroutinesApi::class)
class BluetoothViewModel : ViewModel() {
    val error = MutableLiveData<Throwable>()

    private var btSocket: Deferred<BluetoothSocket> = reconnect()

    val socket = viewModelScope.actor<Pair<Char, Char>>(IO, Channel.CONFLATED) {
        consumeEach {
            Timber.v("$it")
            try {
                btSocket.await().outputStream.write("${it.first}${it.second}\n".toByteArray())
            } catch (t: Throwable) {
                error(t)
                btSocket = reconnect()
            }
        }
    }

    fun reconnect() = viewModelScope.async(IO, CoroutineStart.LAZY) {
        val adapter = BluetoothAdapter.getDefaultAdapter()

        adapter.bondedDevices.forEach {
            Timber.v(it.name)
            it.uuids.forEach { uuid -> Timber.v("    $uuid") }
        }

        adapter.bondedDevices.first().createRfcommSocketToServiceRecord(SPP).apply {
            adapter.cancelDiscovery()
            try {
                connect()
            } catch (t: Throwable) {
                error(t)
            }
        }
    }

    private fun error(t: Throwable) {
        Timber.w(t)
        error.postValue(t)
    }

    companion object {
        private val SPP = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB")
        private const val CHARS = 'Z' - 'A'

        fun Float.toTreadChar(): Char {
            require(this <= 1 && this >= -1)
            return when {
                this > 0 -> {
                    'A' + (this * CHARS).toInt()
                }
                this < 0 -> {
                    'a' + (-this * CHARS).toInt()
                }
                else -> {
                    '0'
                }
            }
        }
    }
}