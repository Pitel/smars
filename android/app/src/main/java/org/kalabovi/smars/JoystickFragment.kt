package org.kalabovi.smars

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import org.kalabovi.smars.BluetoothViewModel.Companion.toTreadChar
import timber.log.Timber
import kotlin.math.abs

class JoystickFragment : Fragment(R.layout.joystick) {
    private val btvm: BluetoothViewModel by activityViewModels()

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        view.setOnTouchListener { v, event ->
            //Timber.v("$event")
            when(event.action) {
                MotionEvent.ACTION_DOWN -> true
                MotionEvent.ACTION_MOVE -> {
                    // http://home.kendra.com/mauser/Joystick.html
                    val x = (event.x - v.width / 2) / (v.width / 2) * -100
                    val y = (v.height / 2 - event.y) / (v.height / 2) * 100
                    val v = (100 - abs(x)) * (y / 100) + y
                    val w = (100 - abs(y)) * (x / 100) + x
                    val r = (v + w) / 2
                    val l = (v - w) / 2
                    Timber.v("$x; $y -> $l; $r")
                    btvm.socket.offer((l / 100).toTreadChar() to (r / 100).toTreadChar())
                    true
                }
                MotionEvent.ACTION_UP -> {
                    btvm.socket.offer('0' to '0')
                    true
                }
                 else -> false
            }
        }
    }
}