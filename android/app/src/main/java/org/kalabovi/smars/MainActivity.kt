package org.kalabovi.smars

import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.content.Intent
import android.os.Bundle
import android.os.StrictMode
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.observe
import com.google.android.material.tabs.TabLayoutMediator
import kotlinx.android.synthetic.main.pager.*
import timber.log.Timber

class MainActivity : AppCompatActivity() {
    private val btvm: BluetoothViewModel by viewModels()

    init {
        if (BuildConfig.DEBUG) {
            StrictMode.setThreadPolicy(
                StrictMode.ThreadPolicy.Builder()
                    .detectAll()
                    .penaltyLog()
                    .build()
            )
            StrictMode.setVmPolicy(
                StrictMode.VmPolicy.Builder()
                    .detectAll()
                    .penaltyLog()
                    .build()
            )
        }
        FragmentManager.enableDebugLogging(true)
        Timber.plant(Timber.DebugTree())
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.pager)
        pager.adapter = PagerAdapter(this)
        pager.isUserInputEnabled = false
        TabLayoutMediator(tabs, pager) { tab, position ->
            tab.text = when (position) {
                0 -> getString(R.string.joystick)
                1 -> getString(R.string.treads)
                else -> "$position"
            }
        }.attach()
    }


    override fun onResume() {
        super.onResume()
        if (!BluetoothAdapter.getDefaultAdapter().isEnabled) {
            registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
                if (it.resultCode == Activity.RESULT_OK) {
                    onBluetoothReady()
                } else {
                    finish()
                }
            }.launch(Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE))
        } else {
            onBluetoothReady()
        }
    }

    private fun onBluetoothReady() {
        Timber.i(Throwable(toString()), "BT ready!")
        btvm.error.observe(this) {
            if (it != null) {
                Toast.makeText(this, it.localizedMessage, Toast.LENGTH_LONG).show()
                btvm.error.value = null
            }
        }
        btvm.socket.offer('0' to '0')
    }
}
