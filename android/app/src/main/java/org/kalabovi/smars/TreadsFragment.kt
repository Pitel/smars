package org.kalabovi.smars

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.google.android.material.slider.Slider
import kotlinx.android.synthetic.main.treads.*
import org.kalabovi.smars.BluetoothViewModel.Companion.toTreadChar

class TreadsFragment : Fragment(R.layout.treads), Slider.OnChangeListener {
    private val btvm: BluetoothViewModel by activityViewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        left.addOnChangeListener(this)
        right.addOnChangeListener(this)
        stop.setOnClickListener {
            left.value = 0f
            right.value = 0f
        }
    }

    override fun onStart() {
        super.onStart()
        stop.callOnClick()
    }

    override fun onStop() {
        stop.callOnClick()
        super.onStop()
    }

    override fun onValueChange(slider: Slider, value: Float, fromUser: Boolean) {
        btvm.socket.offer(left.value.toTreadChar() to right.value.toTreadChar())
    }
}