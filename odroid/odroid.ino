#include <BluetoothSerial.h>
#include <odroid_go.h>

BluetoothSerial serialBT;

float stp;
uint8_t w2, h2;
char l, r;

int8_t char2height(char c) {
  if (c >= 'A' && c <= 'Z') {
    return stp * (c - 'A' + 1);
  } else if (c >= 'a' && c <= 'z') {
    return -stp * (c - 'a' + 1);
  }
  return 0;
}

void left(char c) {
  int8_t h = char2height(c);
  //GO.lcd.print('l');
  //GO.lcd.println(h);
  if (h < 0) {
    GO.lcd.fillRect(0, h2, w2, -h, TFT_WHITE);
  } else {
    GO.lcd.fillRect(0, h2 - h, w2, h, TFT_WHITE);
  }
}

void right(char c) {
  int8_t h = char2height(c);
  //GO.lcd.print('r');
  //GO.lcd.println(h);
  if (h < 0) {
    GO.lcd.fillRect(w2, h2, w2, -h, TFT_WHITE);
  } else {
    GO.lcd.fillRect(w2, h2 - h, w2, h, TFT_WHITE);
  }
}

void setup() {
  GO.begin();

  w2 = GO.lcd.width() / 2;
  h2 = GO.lcd.height() / 2;
  stp = h2 / ('z' - 'a' + 1.0);

  serialBT.begin("SMARS");
  Serial.println("The device started, now you can pair it with bluetooth!");
  GO.lcd.setBrightness(0xf);
  GO.lcd.print("SMARS");
  //left('c');
  //right('C');
}

void loop() {
  if (serialBT.available()) {
    char c = serialBT.read();
    Serial.write(c);
    Serial.write(' ');
    if (l == 0 && r == 0 && c != '\n') {
      Serial.println("Got L");
      l = c;
    } else if (l != 0 && r == 0 && c != '\n') {
      Serial.println("Got R");
      r = c;
    } else if (l != 0 && r != 0 && c == '\n') {
      Serial.write("Got all, drawing");
      GO.lcd.clear();
      //GO.lcd.print(l);
      //GO.lcd.print(r);
      left(l);
      right(r);
      l = 0;
      r = 0;
      if (serialBT.available() > 3) {
        Serial.println(" flush");
        serialBT.flush();
      }
    } else {
      Serial.println("ERROR");
      l = 0;
      r = 0;
    }
  }
}
